#Create a virtual environment for a project:
```$ pip install virtualenv```

```$ cd project_folder```

```$ virtualenv venv```

#activate virtualenv
```source venv/bin/activate #activate```

# install from requirements.txt
```pip install -r requirements.txt```

#DB Migration flask_migrate

creates folder structures

```python manage.py db init ``` 

 populates the migration script with the detected changes in the models

```python manage.py db migrate``` 

 apply the migration to the database
 
```python manage.py db upgrade``` 

# Docker build and run

```docker build -t product .```

```docker run --name product_task -d -p 80:1111 product```

# API address

```http://localhost/api/```
