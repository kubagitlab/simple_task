from flask import request
from flask_restplus import Resource

from .restplus import api
from .serializers import product
from ..controller.product import create_product, update_product, delete_product
from ..models import Product

ns_product = api.namespace('products', description='Operations related to products')


@ns_product.route('/')
class ProductCollection(Resource):

    @api.marshal_list_with(product)
    def get(self):
        """
        Returns list of products.
        """
        products = Product.query.all()
        return products

    @api.response(201, 'Product successfully created.')
    @api.expect(product)
    def post(self):
        """
        Creates a new product.
        """
        data = request.json
        create_product(data)
        return None, 201


@ns_product.route('/<int:id>')
@api.response(404, 'Product not found.')
class ProductItem(Resource):

    @api.marshal_with(product)
    def get(self, id):
        """
        Returns a product
        """
        return Product.query.filter(Product.id == id).one()

    @api.expect(product)
    @api.response(204, 'Product successfully updated.')
    def put(self, id):
        """
        Updates a product.

        Use this method to change the barcode of a product.

        * Send a JSON object with the new barcode in the request body.

        ```
        {
          "barcode": "New Product Barcode"
        }
        ```

        * Specify the ID of the product to modify in the request URL path.
        """
        data = request.json
        update_product(id, data)
        return None, 204

    @api.response(204, 'Product successfully deleted.')
    def delete(self, id):
        """
        Deletes product.
        """
        delete_product(id)
        return None, 204
