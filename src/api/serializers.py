from flask_restplus import fields
from src.api.restplus import api

product = api.model('Product', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of product'),
    'barcode': fields.String(required=True, description='Product barcode'),
})

# product_item = api.inherit('Product item', product, {
#     'posts': fields.List(fields.Nested(blog_post))
# })
