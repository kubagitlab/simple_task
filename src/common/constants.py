# coding=utf-8
ROLE_SUPER_ADMIN = 100
ROLE_ADMIN = 101
ROLE_OPERATOR = 102
ROLE_CONSTRUCTOR = 103

ROLE_SUPPLIER = 1
ROLE_PURCHASER = 2

ROLES = {
    ROLE_SUPER_ADMIN: u'Супер Админ',
    ROLE_ADMIN: u'Админ',
    ROLE_OPERATOR: u'Оператор/Супервизор',
    ROLE_SUPPLIER: u'Поставщик',
    ROLE_PURCHASER: u'Закупщик',
    ROLE_CONSTRUCTOR: u'Конструктор'
}

COMPANY_STATUS_DRAFT = 'draft'
COMPANY_STATUS_WAITING = 'waiting'
COMPANY_STATUS_CONFIRMED = 'confirmed'

ADVERT_STATUS_DRAFT = 'draft'
ADVERT_STATUS_PUBLISHED = 'published'

DIR_NAMES = ['DirBrand', 'DirCountry', 'DirMeasureUnit', 'DirManufacture']
DIR_KEYS = {'DirBrand': 'brand', 'DirCountry': 'country', 'DirMeasureUnit': 'measure_unit',
            'DirManufacture': 'manufacture'}
DT_FORMAT_TZ = '%Y-%m-%dT%H:%M:%S'
DT_FORMAT_TH = '%Y-%m-%d %H:%M'
