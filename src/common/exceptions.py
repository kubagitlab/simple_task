# coding=utf-8

from .error_codes import ERRORS, USER_TYPE_ACCESS_RESTRICTED, USER_LOW_ACCESS_LEVEL, USER_NO_READ_ACCESS, \
    USER_NO_WRITE_ACCESS, USER_NO_DELETE_ACCESS


class CoreException(Exception):
    code = None

    def __init__(self, error_code, message=None, extra=None):
        self.code = error_code
        if message:
            self.message = message
        elif error_code in ERRORS:
            self.message = ERRORS[error_code]
        else:
            self.message = 'Unknown error with code {0}'.format(error_code)

    def __str__(self):
        return repr(self.code)


class AccessRestricted(CoreException):
    def __init__(self):
        super(AccessRestricted, self).__init__(USER_TYPE_ACCESS_RESTRICTED, ERRORS[USER_TYPE_ACCESS_RESTRICTED])


class LowAccessLevel(CoreException):
    def __init__(self, level):
        super(LowAccessLevel, self).__init__(USER_LOW_ACCESS_LEVEL, ERRORS[USER_LOW_ACCESS_LEVEL].format(level))


class NoReadAccess(CoreException):
    def __init__(self):
        super(NoReadAccess, self).__init__(USER_NO_READ_ACCESS)


class NoWriteAccess(CoreException):
    def __init__(self):
        super(NoWriteAccess, self).__init__(USER_NO_WRITE_ACCESS)


class NoDeleteAccess(CoreException):
    def __init__(self):
        super(NoDeleteAccess, self).__init__(USER_NO_DELETE_ACCESS)
