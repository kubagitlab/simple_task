# coding: utf-8

import decimal
import importlib
import json
import os
import re
from datetime import datetime, time, timedelta
from decimal import Decimal, getcontext
from hashlib import md5

import requests
from flask import Response

from . import error_codes
from .exceptions import CoreException
from .json_encoder import orm_to_json as _orm_to_json, json_to_orm as _json_to_orm, JSONEncoderCore as _encoder
from ..config import app_config

orm_to_json = _orm_to_json

json_to_orm = _json_to_orm

JSONEncoderCore = _encoder

GENERIC_DOMAINS = "aero", "asia", "biz", "cat", "com", "coop", \
                  "edu", "gov", "info", "int", "jobs", "mil", "mobi", "museum", \
                  "name", "net", "org", "pro", "tel", "travel"


def datetime_from_str(time_str):
    """Return (<scope>, <datetime.datetime() instance>) for the given
    datetime string.

    >>> _datetime_from_str("2009")
    ('year', datetime.datetime(2009, 1, 1, 0, 0))
    >>> _datetime_from_str("2009-12")
    ('month', datetime.datetime(2009, 12, 1, 0, 0))
    >>> _datetime_from_str("2009-12-25")
    ('day', datetime.datetime(2009, 12, 25, 0, 0))
    >>> _datetime_from_str("2009-12-25 13")
    ('hour', datetime.datetime(2009, 12, 25, 13, 0))
    >>> _datetime_from_str("2009-12-25 13:05")
    ('minute', datetime.datetime(2009, 12, 25, 13, 5))
    >>> _datetime_from_str("2009-12-25 13:05:14")
    ('second', datetime.datetime(2009, 12, 25, 13, 5, 14))
    >>> _datetime_from_str("2009-12-25 13:05:14.453728")
    ('microsecond', datetime.datetime(2009, 12, 25, 13, 5, 14, 453728))
    """
    import time
    import datetime

    formats = [
        # <scope>, <pattern>, <format>
        ("year", "YYYY", "%Y"),
        ("month", "YYYY-MM", "%Y-%m"),
        ("day", "YYYY-MM-DD", "%Y-%m-%d"),
        ("day", "DD.MM.YYYY", "%d.%m.%Y"),
        ("hour", "YYYY-MM-DD HH", "%Y-%m-%d %H"),
        ("minute", "YYYY-MM-DD HH:MM", "%Y-%m-%d %H:%M"),
        ("second", "YYYY-MM-DD HH:MM:SS", "%Y-%m-%d %H:%M:%S"),
        # ".<microsecond>" at end is manually handled below
        ("microsecond", "YYYY-MM-DD HH:MM:SS", "%Y-%m-%d %H:%M:%S"),
    ]
    for scope, pattern, format in formats:
        if scope == "microsecond":
            # Special handling for microsecond part. AFAIK there isn't a
            # strftime code for this.
            if time_str.count('.') != 1:
                continue
            time_str, microseconds_str = time_str.split('.')
            try:
                microsecond = int((microseconds_str + '000000')[:6])
            except ValueError:
                continue
        try:
            # This comment here is the modern way. The subsequent two
            # lines are for Python 2.4 support.
            # t = datetime.datetime.strptime(time_str, format)
            t_tuple = time.strptime(time_str, format)
            t = datetime.datetime(*t_tuple[:6])
        except ValueError:
            pass
        else:
            if scope == "microsecond":
                t = t.replace(microsecond=microsecond)
            return scope, t
    else:
        raise ValueError("could not determine date from %r: does not "
                         "match any of the accepted patterns ('%s')"
                         % (time_str, "', '".join(s for s, p, f in formats)))


def is_valid_ipv4(ip):
    """Validates IPv4 addresses.
    """
    pattern = re.compile(r"""
        ^
        (?:
          # Dotted variants:
          (?:
            # Decimal 1-255 (no leading 0's)
            [3-9]\d?|2(?:5[0-5]|[0-4]?\d)?|1\d{0,2}
          |
            0x0*[0-9a-f]{1,2}  # Hexadecimal 0x0 - 0xFF (possible leading 0's)
          |
            0+[1-3]?[0-7]{0,2} # Octal 0 - 0377 (possible leading 0's)
          )
          (?:                  # Repeat 0-3 times, separated by a dot
            \.
            (?:
              [3-9]\d?|2(?:5[0-5]|[0-4]?\d)?|1\d{0,2}
            |
              0x0*[0-9a-f]{1,2}
            |
              0+[1-3]?[0-7]{0,2}
            )
          ){0,3}
        |
          0x0*[0-9a-f]{1,8}    # Hexadecimal notation, 0x0 - 0xffffffff
        |
          0+[0-3]?[0-7]{0,10}  # Octal notation, 0 - 037777777777
        |
          # Decimal notation, 1-4294967295:
          429496729[0-5]|42949672[0-8]\d|4294967[01]\d\d|429496[0-6]\d{3}|
          42949[0-5]\d{4}|4294[0-8]\d{5}|429[0-3]\d{6}|42[0-8]\d{7}|
          4[01]\d{8}|[1-3]\d{0,9}|[4-9]\d{0,8}
        )
        $
    """, re.VERBOSE | re.IGNORECASE)
    return pattern.match(ip) is not None


def is_valid_ipv6(ip):
    """Validates IPv6 addresses.
    """
    pattern = re.compile(r"""
        ^
        \s*                         # Leading whitespace
        (?!.*::.*::)                # Only a single whildcard allowed
        (?:(?!:)|:(?=:))            # Colon iff it would be part of a wildcard
        (?:                         # Repeat 6 times:
            [0-9a-f]{0,4}           #   A group of at most four hexadecimal digits
            (?:(?<=::)|(?<!::):)    #   Colon unless preceeded by wildcard
        ){6}                        #
        (?:                         # Either
            [0-9a-f]{0,4}           #   Another group
            (?:(?<=::)|(?<!::):)    #   Colon unless preceeded by wildcard
            [0-9a-f]{0,4}           #   Last group
            (?: (?<=::)             #   Colon iff preceeded by exacly one colon
             |  (?<!:)              #
             |  (?<=:) (?<!::) :    #
             )                      # OR
         |                          #   A v4 address with NO leading zeros
            (?:25[0-4]|2[0-4]\d|1\d\d|[1-9]?\d)
            (?: \.
                (?:25[0-4]|2[0-4]\d|1\d\d|[1-9]?\d)
            ){3}
        )
        \s*                         # Trailing whitespace
        $
    """, re.VERBOSE | re.IGNORECASE | re.DOTALL)
    return pattern.match(ip) is not None


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata

        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False


getcontext().prec = 51


def round_as_decimal(num, decimal_places=4):
    """Round a number to a given precision and return as a Decimal

    Arguments:
    :param num: number
    :type num: int, float, decimal, or str
    :returns: Rounded Decimal
    :rtype: decimal.Decimal
    """
    precision = '1.{places}'.format(places='0' * decimal_places)
    return Decimal(str(num)).quantize(Decimal(precision), rounding=decimal.ROUND_HALF_EVEN)


def get_new_session_id(tag):
    """Build a new Session ID"""
    t1 = time.time()
    base = md5.new(tag + str(t1))
    sid = tag + '_' + base.hexdigest()
    return sid


def post_outer(url, data=None, timeout=60):
    if not data:
        data = {}
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

    post_data = json.dumps(data, cls=JSONEncoderCore)
    try:
        response = requests.post(url.strip(), data=post_data, headers=headers, timeout=timeout)

        if response.status_code == requests.codes.ok:
            data = json.loads(response.text)
            return data
        raise CoreException(error_codes.GENERIC_ERROR,
                            'REMOTE REQUEST ERROR {}: {}'.format(response.status_code, response.text))
    except (requests.Timeout, requests.ConnectionError):
        raise CoreException(error_codes.GENERIC_ERROR, 'REMOTE REQUEST NETWORK ERROR')


def eds_post(url, data=None, timeout=600):
    config = get_config_type()
    base_url = config.API_ZAKUPKI
    return post_outer(base_url + url, data, timeout)


def eds_get(url, data=None, timeout=600):
    config = get_config_type()
    base_url = config.API_ZAKUPKI
    return get_outer(base_url + url, data, timeout)


def get_outer(url, data=None, timeout=60):
    if not data:
        data = {}
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

    post_data = json.dumps(data, cls=JSONEncoderCore)
    try:
        response = requests.get(url.strip(), data=post_data, headers=headers, timeout=timeout)

        if response.status_code == 200:
            data = json.loads(response.text)
            return data
        raise CoreException(error_codes.GENERIC_ERROR,
                            'REMOTE REQUEST ERROR {}: {}'.format(response.status_code, response.text))
    except (requests.Timeout, requests.ConnectionError):
        raise CoreException(error_codes.GENERIC_ERROR, 'REMOTE REQUEST TIMEOUT')


def make_json_response(p_content=None):
    if p_content is None:
        p_content = {}
    if isinstance(p_content, list):
        p_content = {'list': p_content}
    if 'result' not in p_content:
        p_content.update({'result': 0})

    return Response(json.dumps(p_content, cls=JSONEncoderCore), mimetype='application/json; charset=utf-8')


image_extensions = ['gif', 'png', 'jpg', 'jpeg']
doc_extensions = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'txt']
all_extensions = image_extensions + doc_extensions


def allowed_img_file(filename):
    return check_file_extension(filename, image_extensions)


def allowed_doc_file(filename):
    return check_file_extension(filename, doc_extensions)


def allowed_file(filename):
    return check_file_extension(filename, all_extensions)


def check_file_extension(filename, extensions):
    if not filename:
        return False
    f_name, ext = os.path.splitext(filename)
    if not ext:
        return False
    ext = ext.replace('.', '')
    return ext.lower() in extensions


def get_path(path, file_name=None):
    upload_path = os.getenv('UPLOAD_PATH')
    dir_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..', upload_path, path))
    thumb_dir_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..', upload_path, path, 'thumb'))
    os.makedirs(thumb_dir_path, exist_ok=True)
    path = os.path.join('app', dir_path, file_name) if file_name else dir_path
    thumb_path = os.path.join('app', thumb_dir_path, file_name) if file_name else thumb_dir_path
    return path, thumb_path


def get_config_type(param='FLASK_ENV'):
    env_name = os.getenv(param)
    return app_config[env_name]


def get_config(param=None):
    if not param:
        return param
    configs = orm_to_json(get_config_type())
    return configs.get(param, '')


def is_dev():
    env_config = get_config_type()
    return env_config.DEBUG


def get_datetime(date_string, date_format='%Y-%m-%d', time_zone=False):
    dt = datetime.strptime(date_string, date_format)
    if time_zone:
        dt = dt + timedelta(hours=6)
    return dt


def get_model(table_name):
    module = importlib.import_module('src.models')
    model = getattr(module, table_name)
    return model


def get_model_all(table_name):
    model = get_model(table_name)
    return model.all()


def get_model_by(table_name, id):
    model = get_model(table_name)
    return model.get(id)


def get_dir_value(table_name, id):
    dir_table = get_model_by(table_name, id)
    return dir_table.name if dir_table else ''
