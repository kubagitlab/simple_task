from flask import g
from sqlalchemy import text, null
from sqlalchemy.orm import aliased

from src.common.constants import *
from src.common.error_codes import GENERIC_ERROR
from src.common.utils import *
from src.models import *


def create_product(data):
    product = Product()
    product.barcode = data.get('barcode', '')

    db.session.add(product)
    db.session.commit()


def update_product(product_id, data):
    product = Product.query.filter(Product.id == product_id).one()
    product.barcode = data.get('barcode')
    db.session.add(product)
    db.session.commit()


def delete_product(product_id):
    product = Product.query.filter(Product.id == product_id).one()
    db.session.delete(product)
    db.session.commit()
