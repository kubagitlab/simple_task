import logging
import logging.handlers
import os
import traceback
from time import time

from flask import Flask, Blueprint
from flask import request, g
from sqlalchemy.exc import IntegrityError

from .api.product import ns_product
from .api.restplus import api
from .common.error_codes import ERRORS, SYSTEM_FAILURE
from .common.exceptions import CoreException
from .common.utils import make_json_response, get_config_type
from .models import db


def create_app():
    app = Flask(__name__)
    env_config = get_config_type()
    app.config.from_object(env_config)

    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(ns_product)
    app.register_blueprint(blueprint)

    db.init_app(app)
    # logger_init()

    @app.before_request
    def before():
        if request.method == 'OPTIONS':
            return
        g.time = time()

    @app.teardown_request
    @app.errorhandler(Exception)
    @app.errorhandler(500)
    def after(exception):
        if request.method == 'OPTIONS':
            return

        if hasattr(g, 'time'):
            logging.info('Request:{} finished in {} sec'.format(request.url.encode('utf-8'), time() - g.time))

        if exception:
            db.session.rollback()
            logging.exception(traceback.format_exc())
            if isinstance(exception, CoreException):
                return make_json_response({'message': exception.message, 'result': exception.code})

            try:
                message = exception.message
            except:
                message = str(exception)

            if isinstance(exception, KeyError):
                return make_json_response({'message': u'Not found {}'.format(message), 'result': -1})

            if isinstance(exception, IntegrityError):
                try:
                    m = exception.orig.pgerror
                    return make_json_response({'message': m, 'result': -1})
                except:
                    return make_json_response({'message': message, 'result': -1})

            return make_json_response({'message': ERRORS[SYSTEM_FAILURE], 'result': -1})

        else:
            db.session.commit()
        db.session.remove()

    @app.route('/', methods=['GET'])
    def index():
        return make_json_response({'response': 'SUCCESS!'})

    return app


def logger_init():
    path = 'log'
    os.makedirs(path, exist_ok=True)
    f = logging.Formatter(fmt='%(name)s; %(asctime)s; %(levelname)s; %(filename)s %(lineno)d: %(message)s ',
                          datefmt="%Y-%m-%d %H:%M:%S")
    handlers = [
        logging.handlers.TimedRotatingFileHandler(os.path.join(path, 'daily.log'), when="midnight", interval=1),
        logging.StreamHandler()
    ]
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    logging.getLogger('chardet.charsetprober').setLevel(logging.INFO)

    for h in handlers:
        h.setFormatter(f)
        h.suffix = "%Y%m%d"
        h.setLevel(logging.DEBUG)
        root_logger.addHandler(h)
