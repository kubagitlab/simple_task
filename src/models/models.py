from . import db
from .BaseModel import BaseModel


class Product(BaseModel):
    __tablename__ = 'product'

    barcode = db.Column(db.String(128), unique=True)
