from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from src.app import create_app, db

app = create_app()

migrate = Migrate(app=app, db=db)

manager = Manager(app=app)
manager.add_command('db', MigrateCommand)


@manager.command
def init_db():
    print('init start')


if __name__ == '__main__':
    manager.run()
